# Test File directory
This directory should contain all files your tests want.
The directory itself will be in the same location as the executable being tested
Tests themselves should be put into the `test_scripts` directory and should return 0 on success and nonzero on failure.
